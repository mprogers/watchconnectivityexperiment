//
//  File.swift
//  WatchkitConnectivityExperiment
//
//  Created by Michael Rogers on 2/19/19.
//  Copyright © 2019 Michael Rogers. All rights reserved.
//

import Foundation
import WatchKit


// A simple table row controller

class CityTableRowController:NSObject {
    
    @IBOutlet weak var cityLBL: WKInterfaceLabel!
    
}
