//
//  InterfaceController.swift
//  WatchkitConnectivityExperiment WatchKit Extension
//
//  Created by Michael Rogers on 2/17/19.
//  Copyright © 2019 Michael Rogers. All rights reserved.
//

import WatchKit
import WatchConnectivity
import Foundation


class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
    var session:WCSession!
    let messageToSendToiPhone = ["countries":["Peru", "Ecuador", "Brazil", "New Zealand", "Sudan"]]

    @IBOutlet weak var citiesTBL: WKInterfaceTable!
    
    
    
    // Invoked when we click on Send Message button
    
    @IBAction func sendToiPhone() {
        
        session.sendMessage(messageToSendToiPhone, replyHandler: nil, errorHandler: {error in
            print("Got an error in sendMessage(): \(error)")
        })
        print("Message sent from Apple Watch to iPhone!")
        
    }
    
    // WCSession methods, this time on the watch
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("Hey, I (Apple Watch) got a message: \(message)")
        if let cities = message["countries"] as? [String] {
            citiesTBL.setNumberOfRows(cities.count, withRowType: "city")
            
            for i in 0 ..< citiesTBL.numberOfRows {
                let theRow = citiesTBL.rowController(at: i) as! CityTableRowController
                theRow.cityLBL.setText(cities[i])
            }
        }
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("Session activated on Apple Watch")
    }
    
    func sessionDidDeactivate(session: WCSession) {
        print("Session deactivated on Apple Watch!")
        
    }
    
    func sessionDidBecomeInactive(session: WCSession) {
        print("Session became inactive on Apple Watch!")
    }
    
    func sessionReachabilityDidChange(_ session: WCSession) {
        print("Reachability on Apple Watch changed to \(session.isReachable)")
        
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        session = WCSession.default // set up the session in the standard way
        session.delegate = self
        session.activate()
        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
}
