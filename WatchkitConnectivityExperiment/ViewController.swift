//
//  ViewController.swift
//  WatchkitConnectivityExperiment
//
//  Created by Michael Rogers on 2/17/19.
//  Copyright © 2019 Michael Rogers. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate, UITableViewDataSource, UITableViewDelegate {
    
    var session:WCSession!
    
    @IBOutlet weak var countriesTV: UITableView!
    
    
    // messages must be in the form of dictionaries
    let messageToSendToWatch = ["countries":["Canada", "United Kingdom", "Australia", "India", "Ukraine"]]
    
    var countries:[String] = [] // data source for the table -- we will retrieve this from the watch
    
    
    // UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "country") {
            cell.textLabel?.text = countries[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    
    
    
    
    // Called when we want to send something to the watch
    @IBAction func sendToAppleWatch(_ sender: Any) {
        
        session.sendMessage(messageToSendToWatch, replyHandler: nil, errorHandler: {error in
            print("Got an error in sendMessage(): \(error)")
        })
        print("Message sent to Apple Watch!")
        
    }
    
    
    // Several self-explanatory WCSessionDelegate methods
    
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("Hey, I (iOS App) got something: \(message)")
        countries = message["countries"] as! [String]
        DispatchQueue.main.async {
            self.countriesTV.reloadData() // session(_:didReceiveMessage:) runs in a background thread. But if you want to interact with a UI element, you must do so on the main thread. That's what this does.
        }
        
    }
        
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("Session activated on iPhone")
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        print("Session deactivated on iPhone!")
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        print("Session became inactive on iPhone!")
    }
    
    func sessionReachabilityDidChange(_ session: WCSession) {
        print("Reachability changed to \(session.isReachable) on iPhone")
        
    }
    
    
    // Usual life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        session = WCSession.default // access the WCSession for the iPhone
        session.delegate = self
        session.activate()          // ... and activate it
    }
    
}

